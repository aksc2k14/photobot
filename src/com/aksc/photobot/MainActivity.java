package com.aksc.photobot;



import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;


import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	Context c;
	Camera mCamera;
	MediaPlayer beep,click;
	SurfaceView cameraSurface;
	SurfaceHolder cameraHolder;
	OverlaySurface overlay;
	int mHeight,mWidth;
	TextView tv;
	public static TextView tv1;
	Size size;
	List<Size> pictures;
	int rec[] = {0,0,0,0};
	Rect rect;
	URL url=null;
	UploadThread uploadThread;
	WakeLock wakeLock;
	String ip = "http://192.168.0.18/photobot/";
	String dataShareUrl = ip+"android.php?q=";
	public static String res = null;
	int center=0;
	HTTPSync syn;
	boolean sleep = false;
	long timeTaken;
	String command;
	String prevCommand;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main);
		c = this;
		
		beep = MediaPlayer.create(this, R.raw.beep);
		click = MediaPlayer.create(this, R.raw.camerashutterclick);

		cameraSurface = (SurfaceView) findViewById(R.id.cameraSurface);
		cameraHolder = cameraSurface.getHolder();

        cameraHolder.addCallback(cameraCallback);
		overlay = (OverlaySurface) findViewById(R.id.overSurface);
		
		Display d = getWindowManager().getDefaultDisplay();
		Point outSize = new Point();
		d.getSize(outSize);
		mWidth = outSize.x;
		mHeight = outSize.y;
		
		tv = (TextView) findViewById(R.id.textView1);
		tv1 = (TextView) findViewById(R.id.textView2);
		
		rect = new Rect(0,0,0,0);
		
		try {
			url = new URL(ip+"upload.php");
			//tv1.setText(url.toString());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		uploadThread = new UploadThread(tv1,url);
		uploadThread.start();
		
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
		        "MyWakelockTag");
		wakeLock.acquire();
	
		syn = new HTTPSync();
		syn.execute(dataShareUrl);
		//syncHttp("hhhh");

	}
	

	private SurfaceHolder.Callback cameraCallback = new SurfaceHolder.Callback() {
		
		@Override
		public void surfaceDestroyed(SurfaceHolder arg0) {
			// TODO Auto-generated method stub
			if(mCamera==null)
				return;
			mCamera.stopFaceDetection();
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
		
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			// TODO Auto-generated method stub
		
			try {
				Camera.CameraInfo info = new Camera.CameraInfo();
				int cameraId = 0;
				int cameraCount = Camera.getNumberOfCameras();
				for(int i=0;i<cameraCount;i++){
					Camera.getCameraInfo(i, info);
					if(info.facing == Camera.CameraInfo.CAMERA_FACING_BACK){
						cameraId = i;
						break;
					}
				}
				mCamera = Camera.open(cameraId);
				mCamera.setFaceDetectionListener(faceDetectionListener);
				
				
				mCamera.setPreviewDisplay(holder);
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				if(mCamera == null)
					return;
				mCamera.release();
				mCamera = null;
			}
			
		}
		
		@Override
		public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
			// TODO Auto-generated method stub
			Camera.Parameters params = mCamera.getParameters();
			params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
			size = params.getPictureSize();
			pictures = params.getSupportedPictureSizes();
			mCamera.setParameters(params);
			mCamera.startPreview();
			mCamera.startFaceDetection();
			tv.setText(size.width+" "+size.height+"\n");
				
		}
	};
	
	private FaceDetectionListener faceDetectionListener = new FaceDetectionListener() {
		
		@Override
		public void onFaceDetection(Face[] faces, Camera cam) {
			// TODO Auto-generated method stub

			
			try {	
				
					center = faces[0].rect.centerX();					
			
					
				
					
					if(syn.getStatus()==AsyncTask.Status.FINISHED){
						syn = new HTTPSync();
						if(center<-100){
							command = "LEFT";
							prevCommand = command;
						}
						else if(center>100){
							command="RIGHT";
							prevCommand = command;
						}else{
							command = "GO";
							prevCommand = command;
							
						}
						syn.execute(dataShareUrl+command);
					}
					
					
					faces[0].rect.left = (faces[0].rect.left+1000)*mWidth/2000;
					faces[0].rect.top = (faces[0].rect.top+1000)*mHeight/2000;
					faces[0].rect.right = (faces[0].rect.right+1000)*mWidth/2000;
					faces[0].rect.bottom = (faces[0].rect.bottom+1000)*mHeight/2000;
					
					overlay.rect = faces[0].rect;
					overlay.isFace = true;
					
					if(center>-100&&center<100){
						long now;

						if(((now=System.currentTimeMillis())-timeTaken)>5000){
							mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
							timeTaken = now;
						}
					}else{

						beep.start();
					}
					
				
					
					
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	};
	
	private void resetCam(){
		mCamera.startPreview();
		mCamera.startFaceDetection();
	}
	
	ShutterCallback shutterCallback = new ShutterCallback() {
		
		@Override
		public void onShutter() {
			// TODO Auto-generated method stub
		}
	};
	
	PictureCallback rawCallback = new PictureCallback() {
		
		@Override
		public void onPictureTaken(byte[] arg0, Camera arg1) {
			// TODO Auto-generated method stub
			
		}
	};
	
	PictureCallback jpegCallback = new PictureCallback() {
		
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			// TODO Auto-generated method stub
			new SaveImageTask(MainActivity.this).execute(data);
			resetCam();
		}
	};
	
	
	
	@Override
	protected void onPause() {
		super.onPause();
		if(mCamera!=null){
			mCamera.stopFaceDetection();
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
		wakeLock.release();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		wakeLock.release();
	}
	
	
	
}


