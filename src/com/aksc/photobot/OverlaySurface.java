package com.aksc.photobot;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.hardware.Camera.Face;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class OverlaySurface extends SurfaceView implements SurfaceHolder.Callback {

	Context con;
	MyThread thread;
	int x=0;
	Paint p;
	Rect rect;
	public int rec[]={0,0,0,0};
	Rect faces[];
	boolean isFace = false;
	
	public OverlaySurface(Context context, AttributeSet attrs) {
		super(context, attrs);
		con = context;
		SurfaceHolder holder = getHolder();
		this.setZOrderOnTop(true);
		holder.setFormat(PixelFormat.TRANSPARENT);
		holder.addCallback(this);
		p = new Paint();
		p.setStyle(Paint.Style.STROKE);
		p.setStrokeWidth(10);
		p.setARGB(100,0,255,0);
		rect = new Rect(0,0,0,0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		thread = new MyThread(con,arg0,this);
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		thread.setRunning(false);
		boolean retry = true;
		while(retry){
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void doDraw(Canvas mCanvas) {
		// TODO Auto-generated method stub
		
		mCanvas.drawColor(0, Mode.CLEAR);
		if(isFace){
			try {
				mCanvas.drawRect(rect, p);
			//	for(int i=0;i<faces.length;i++){
			//		mCanvas.drawRect(faces[i], p);
			//	}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			isFace = false;
		}
	}
	
	

}
