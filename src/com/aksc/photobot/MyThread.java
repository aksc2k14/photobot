package com.aksc.photobot;

import android.content.Context;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class MyThread extends Thread{
	boolean isRunning;
	public OverlaySurface overlay;
	SurfaceHolder surfaceHolder;
	Context c;
	Canvas mCanvas;
	
	public MyThread(Context con, SurfaceHolder holder, OverlaySurface surface) {
		// TODO Auto-generated constructor stub
		c = con;
		isRunning = false;
		surfaceHolder = holder;
		overlay = surface;
	}
	
	void setRunning(boolean b){
		isRunning = b;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		while(isRunning){
			mCanvas = surfaceHolder.lockCanvas();
			if(mCanvas!=null){
				overlay.doDraw(mCanvas);
				surfaceHolder.unlockCanvasAndPost(mCanvas);
			}
		}
	}
	
}
