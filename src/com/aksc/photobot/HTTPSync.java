package com.aksc.photobot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.widget.TextView;



public class HTTPSync extends AsyncTask<String, Void, String>{
	
	TextView tv;

	public HTTPSync() {
		// TODO Auto-generated constructor stub
		tv = MainActivity.tv1;
	}

	@Override
	protected String doInBackground(String... urls) {
		// TODO Auto-generated method stub

		
		shareData(urls[0]);
		
		return null;
	}
	
	Void shareData(String s){
		
		String result="";
		try {
			URI url = new URI(s);
			HttpGet request = new HttpGet(url);
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = client.execute(request);
			InputStream in = response.getEntity().getContent();
			if(in!=null){
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				result+=br.readLine();
			}
			in.close();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if(result!=null&&result!=""&&result!="null")
				MainActivity.res = result;
			tv.setText(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	


}
