package com.aksc.photobot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

@SuppressLint("DefaultLocale")
public class SaveImageTask extends AsyncTask<byte[], Void, Void>{
	
	Context context;

	public SaveImageTask(Context c) {
		// TODO Auto-generated constructor stub
		context = c;
	}
	
	@Override
	protected Void doInBackground(byte[]... data) {
		// TODO Auto-generated method stub
		
		
		FileOutputStream out = null;
		
		try {
			File sdCard = Environment.getExternalStorageDirectory();
			File saveFolder = new File(sdCard, "/Photobot");
			saveFolder.mkdir();
			String fileName = String.format("%d.jpg", System.currentTimeMillis());
			File outFile = new File(saveFolder,fileName);
			out = new FileOutputStream(outFile);
			out.write(data[0]);
			out.flush();
			out.close();
			refreshGallery(saveFolder);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

	private void refreshGallery(File file) {
		// TODO Auto-generated method stub
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		mediaScanIntent.setData(Uri.fromFile(file));
		context.getApplicationContext().sendBroadcast(mediaScanIntent);
	}

}
